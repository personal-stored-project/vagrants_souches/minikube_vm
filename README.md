# Vagrant_to_Minikube

### Automate VM Creation for test Minikube and Kubectl

## Context and Objectives:
Because Minikube need an environement for testing, i made this project for easy provide a realy tiny environement for trying Minikube on a Vm

_**Duration of installation**: 10 minutes_
## Description of the manipulation and the different steps
_**Prerequisites: it is necessary to have a solid base on the Docker environment**_

**Steps**:
- Install Vagrant and Virtualbox
- Download Git repo :
```Powershell
git clone https://gitlab.com/leonardarma/vagrant_to_minikube.git
```
- Launch Vagrant project :
```Powershell
cd vagrant_to_minikube
vagrant up
```
- Connect to the VM:
```Powershell
vagrant ssh
```
- if you want to shutdown VM:
```Posershell
vagrant halt
```
- if you want to start after halting :
```Powershell
vagrant reload
```
- if you want to destroy the VM:
vagrant destroy

## Environement
### Network
Please see Vagrant Network Documentation

### Virtualisation

The virtual machine runs on an ubuntu 22.04, with 2 procs and 8192 MB of ram. The chosen hypervisor is VirtualBox.
Vagrant box: ubuntu/focal64 v20220927.0.1

### Notes
This readme and this project is still in developement. Modifications may occurs.
