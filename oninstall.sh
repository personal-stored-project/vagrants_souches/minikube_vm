echo "---Install Dependencies---"
apt-get update
sudo apt-get -qq upgrade -y


echo "---Install Docker---"
apt-get -qq update


apt-get -qq install ca-certificates curl gnupg lsb-release

mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get -qq update
apt-get -qq install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y


echo "---Install Minikube---"

curl -sLO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
install minikube-linux-amd64 /usr/local/bin/minikube

echo "---Add Vagrant User to Docker Group---"
usermod -aG docker vagrant && newgrp docker

echo "---Make Alias for eazy use Kubectl---"
echo 'alias kubectl="minikube kubectl --"' >> /home/vagrant/.bashrc

echo "---Start Minikube---"
su vagrant -c 'minikube start'

echo "---Ready to Minikube---"